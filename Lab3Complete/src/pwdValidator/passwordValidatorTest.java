/*
 * Trevor D'Souza || 991552924
 * This class takes in input from user and spits out if the password is valid or not and will be created using TDD
 */
package pwdValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class passwordValidatorTest {

	@Test
	public void passwordValidatorLengthRegular() {
		boolean result = passwordValidator.validateLength("hjskqweqe");
		assertTrue("Invalid password", result == true);
	}
	
	@Test (expected=RuntimeException.class)
	public void passwordValidatorLengthException() {
		boolean result = passwordValidator.validateLength("");
		assertFalse("password cannot be empty", result);
	}
	
	@Test (expected=StringIndexOutOfBoundsException.class)
	public void passwordValidatorLengthBoundaryIn() {
		boolean result = passwordValidator.validateLength("jklsjdf");
		assertFalse("Password cannot be under 8 characters", result);
	}
	
	@Test
	public void passwordValidatorLengthBoundaryOut() {
		boolean result = passwordValidator.validateLength("jklsjdfw");
		assertTrue("Invalid password", result == true);
	}
	
	@Test
	public void passwordValidatorNumericsRegular() {
		boolean result = passwordValidator.validateNumerics("kilokj88");
		assertTrue("Invalid password", result == true);
	}
	
	@Test (expected=RuntimeException.class)
	public void passwordValidatorNumericsException() {
		boolean result = passwordValidator.validateNumerics("");
		assertFalse("password cannot be empty", result);
	}
	
	@Test (expected=StringIndexOutOfBoundsException.class)
	public void passwordValidatorNumericsBoundaryIn() {
		boolean result = passwordValidator.validateNumerics("ieoruty8");
		assertFalse("password cannot have less than 2 numeric digits", result);
	}
	
	@Test
	public void passwordValidatorNumericsBoundaryOut() {
		boolean result = passwordValidator.validateNumerics("ieoruty8567");
		assertTrue("Invalid password", result == true);
	}

}
