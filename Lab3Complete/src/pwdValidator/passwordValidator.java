/*
 * Trevor D'Souza || 991552924
 * This class takes in input from user and spits out if the password is valid or not and will be created using TDD
 */
package pwdValidator;

import java.util.Scanner;

public class passwordValidator {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Enter a password: ");
		String password = input.nextLine();
		if(validateLength(password) && validateNumerics(password)) {
			System.out.println("Password is valid");
		} else {
			System.out.println("Password is invalid");
		}
	}

	public static boolean validateLength(String password) throws RuntimeException, StringIndexOutOfBoundsException {
		if(password.length() < 8 && password.length() != 0) {
			throw new StringIndexOutOfBoundsException("Password cannot be under 8 characters");
		} else if (password.length() == 0){
			throw new RuntimeException("Password cannot be empty");
		} else {
			return true;
		}
	}
	
	public static boolean validateNumerics(String password) throws RuntimeException, StringIndexOutOfBoundsException {
		int countNum = 0;
		if(password.isEmpty()) throw new RuntimeException("Password cannot be empty");
		for (char c: password.toCharArray()) {
			if(Character.isDigit(c)) {
				countNum++;
				if(countNum ==2) {
					return true;
				}
			}
		}
		throw new StringIndexOutOfBoundsException("Password cannot have less than 2 numeric digits");
	}

}
